use crate::prelude::*;

pub fn draw(mut a: Point, mut b: Point, mut plot: impl FnMut(i32, i32, f32)) {
    let steep = (b.y - a.y).abs() > (b.x - a.x).abs();

    if steep {
        std::mem::swap(&mut a.x, &mut a.y);
        std::mem::swap(&mut b.x, &mut b.y);
    }

    if a.x > b.x {
        std::mem::swap(&mut a, &mut b)
    }

    let s = {
        let x = (b.x - a.x).abs();
        let y = (b.y - a.y).abs();
        let s = if x > y { y / x } else { x / y };
        1.0 + 0.4 * s * s
    };

    let mut plot = |x, y, v| plot(x, y, v * s);

    let dx = b.x - a.x;
    let gradient = if dx == 0.0 { 1.0 } else { (b.y - a.y) / dx };

    let x_end = a.x.round();
    let y_end = a.y + gradient * (x_end - a.x);
    let x_gap = 1.0 - (a.x + 0.5).fract();

    let x_0 = x_end as i32;
    let y_0 = y_end as i32;

    if steep {
        plot(y_0, x_0, (1.0 - (y_end.fract())) * x_gap);
        plot(y_0 + 1, x_0, y_end.fract() * x_gap);
    } else {
        plot(x_0, y_0, (1.0 - y_end.fract()) * x_gap);
        plot(x_0, y_0 + 1, y_end.fract() * x_gap);
    }

    let mut y_i = y_end + gradient;

    let x_end = b.x.round();
    let y_end = b.y + gradient * (x_end - b.x);
    let x_gap = (b.x + 0.5).fract();

    let x_1 = x_end as i32;
    let y_1 = y_end as i32;

    if steep {
        plot(y_1, x_1, (1.0 - y_end.fract()) * x_gap);
        plot(y_1 + 1, x_1, y_end.fract() * x_gap);
    } else {
        plot(x_1, y_1, (1.0 - y_end.fract()) * x_gap);
        plot(x_1, y_1 + 1, y_end.fract() * x_gap);
    }

    for x in x_0 + 1..x_1 {
        if steep {
            plot(y_i.floor() as i32, x, 1.0 - y_i.fract());
            plot(y_i.floor() as i32 + 1, x, y_i.fract());
        } else {
            plot(x, y_i.floor() as i32, 1.0 - y_i.fract());
            plot(x, y_i.floor() as i32 + 1, y_i.fract());
        }
        y_i += gradient;
    }
}
