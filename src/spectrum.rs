pub fn wavelength_to_rgb(wavelength: f32) -> [f32; 3] {
    if wavelength >= 380.0 && wavelength <= 440.0 {
        let attenuation = 0.3 + 0.7 * (wavelength - 380.0) / (440.0 - 380.0);
        [
            (-(wavelength - 440.0) / (440.0 - 380.0)) * attenuation,
            0.0,
            attenuation,
        ]
    } else if wavelength >= 440.0 && wavelength <= 490.0 {
        [0.0, (wavelength - 440.0) / (490.0 - 440.0), 1.0]
    } else if wavelength >= 490.0 && wavelength <= 510.0 {
        [0.0, 1.0, -(wavelength - 510.0) / (510.0 - 490.0)]
    } else if wavelength >= 510.0 && wavelength <= 580.0 {
        [(wavelength - 510.0) / (580.0 - 510.0), 1.0, 0.0]
    } else if wavelength >= 580.0 && wavelength <= 645.0 {
        [1.0, -(wavelength - 645.0) / (645.0 - 580.0), 0.0]
    } else if wavelength >= 645.0 && wavelength <= 750.0 {
        let attenuation = 0.3 + 0.7 * (750.0 - wavelength) / (750.0 - 645.0);
        [attenuation, 0.0, 0.0]
    } else {
        [0.0, 0.0, 0.0]
    }
}
