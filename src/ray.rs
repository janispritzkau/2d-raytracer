use crate::prelude::*;

pub struct Ray {
    pub origin: Point,
    pub direction: Vector
}

pub struct Intersection {
    pub distance: f32,
    pub position: Point,
    pub normal: Vector
}
