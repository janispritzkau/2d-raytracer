extern crate raytracer;

use rand::{Rng, SeedableRng};
use rand_xorshift::XorShiftRng;
use raytracer::prelude::*;
use raytracer::light::Light;
use std::time::Instant;

pub enum Material {
    Diffuse,
    Glass,
    Mirror
}

fn main() {
    let (width, height) = (1200, 800);
    let samples = 1024 * 512;
    let max_bounces = 12;

    let center = Point::new(width as f32 / 2.0, height as f32 / 2.0);

    let intersect = |ray: &Ray| {
        let mut nearest_hit: Option<(Intersection, Material)> = None;
        let mut nearest = std::f32::INFINITY;

        if let Some(hit) = intersect_bbox(&ray, center, center.to_vec()) {
            if hit.distance < nearest {
                nearest = hit.distance;
                nearest_hit = Some((hit, Material::Diffuse));
            }
        }

        if let Some(hit) = intersect_circle(&ray, center, 100.0) {
            if hit.distance < nearest {
                nearest = hit.distance;
                nearest_hit = Some((hit, Material::Glass));
            }
        }

        if let Some(hit) = intersect_circle(&ray, Point::new(200.0, height as f32 / 2.0), 70.0) {
            if hit.distance < nearest {
                nearest_hit = Some((hit, Material::Glass));
            }
        }

        nearest_hit
    };

    let mut pixels: Vec<[f32; 3]> = vec![[0.0; 3]; width * height];
    let mut rng = XorShiftRng::seed_from_u64(2034578901);

    let beam = raytracer::light::Beam {
        start: Point::new(width as f32 - 1.0, height as f32 / 2.0 + 65.0),
        direction: -Vector::unit_x(),
        size: 9.0
    };

    let start = Instant::now();
    let mut rays = 0;

    for _ in 0..samples {
        let (mut ray, e) = beam.sample(&mut rng);
        let w = rng.gen_range(350.0, 700.0);
        let u = raytracer::spectrum::wavelength_to_rgb(w);

        for _ in 0..max_bounces {
            if let Some((hit, material)) = intersect(&ray) {
                rays += 1;
                raytracer::line::draw(ray.origin, hit.position, |x, y, c| {
                    if x > 0 && x < width as _ && y > 0 && y < height as _ {
                        let brightness = c / (samples + 64) as f32 * 280.0 * e;
                        let pixel = &mut pixels[y as usize * width + x as usize];
                        pixel[0] += u[0] * brightness * 0.9;
                        pixel[1] += u[1] * brightness * 0.9;
                        pixel[2] += u[2] * brightness;
                    }
                });

                ray.origin = hit.position + hit.normal * 0.01;

                match material {
                    Material::Diffuse => {
                        let angle = hit.normal.y.atan2(hit.normal.x) + rng.gen_range(-PI, PI);
                        ray.direction = Vector::new(angle.cos(), angle.sin());
                        if rng.gen::<f32>() > 0.7 { break }
                    }
                    Material::Glass => {
                        // TODO: Find better formula for dispersion
                        let mut etai = 1.0 / (1.53 - w / 10000.0);
                        let mut etat = 1.0;

                        let n = hit.normal * if ray.direction.dot(hit.normal) > 0.0 {
                            -1.0
                        } else {
                            std::mem::swap(&mut etai, &mut etat);
                            1.0
                        };

                        let reflected = reflect(n, ray.direction);
                        let refracted = refract(n, ray.direction, etat / etai);

                        let r1 = reflected.dot(n);
                        let r2 = refracted.dot(-n);
                        let f = 1.0 - (
                            ((etat * r1 - etai * r2) / (etat * r1 + etai * r2)).powi(2) +
                            ((etai * r2 - etat * r1) / (etai * r2 + etat * r1)).powi(2)
                        ) / 2.0;

                        ray = if f < rng.gen::<f32>() {
                            Ray {
                                origin: hit.position + n * 0.01,
                                direction: reflected
                            }
                        } else {
                            Ray {
                                origin: hit.position - n * 0.01,
                                direction: refracted
                            }
                        };

                        if rng.gen::<f32>() > 0.96 { break }
                    }
                    Material::Mirror => {
                        ray.direction = reflect(hit.normal, ray.direction);
                        if rng.gen::<f32>() > 0.8 { break }
                    }
                }
            } else {
                break
            }
        }
    }

    let elapsed = start.elapsed();

    println!("Rendering took {:?}, {} rays traced", elapsed, rays);

    image::save_buffer(
        "output.png",
        &(0..width * height * 3)
            .map(|i| {
                (pixels[i / 3][i % 3].min(1.0).powf(1.0 / 2.4) * 255.0) as u8
            })
            .collect::<Vec<_>>(),
        width as _,
        height as _,
        image::ColorType::RGB(8),
    )
    .unwrap();
}

fn reflect(normal: Vector, direction: Vector) -> Vector {
    direction - 2.0 * normal * normal.dot(direction)
}

fn refract(normal: Vector, direction: Vector, eta: f32) -> Vector {
    let cos_i = -direction.dot(normal);
    let cos_t2 = 1.0 - eta * eta * (1.0 - cos_i * cos_i);
    let t = direction * eta + normal * (eta * cos_i - cos_t2.abs().sqrt());
    t * if cos_t2 > 0.0 { 1.0 } else { 0.0 }
}

fn intersect_bbox(ray: &Ray, center: Point, size: Vector) -> Option<Intersection> {
    let pos = ray.origin - center;

    let tx1 = (-size.x - pos.x) * (1.0 / ray.direction.x);
    let tx2 = ( size.x - pos.x) * (1.0 / ray.direction.x);
    let ty1 = (-size.y - pos.y) * (1.0 / ray.direction.y);
    let ty2 = ( size.y - pos.y) * (1.0 / ray.direction.y);

    let x_min = tx1.min(tx2); let x_max = tx1.max(tx2);
    let y_min = ty1.min(ty2); let y_max = ty1.max(ty2);

    let t_min = x_min.max(y_min);
    let t_max = x_max.min(y_max);

    if t_max < t_min { return None }

    let distance = if t_min <= 0.0 { t_max } else { t_min };
    let normal = if distance == tx1 { Vector::unit_x() } else if distance == tx2 { -Vector::unit_x() } else if distance == ty1 { Vector::unit_y() } else { -Vector::unit_y() };

    if distance < 0.0 { return None }

    Some(Intersection {
        distance,
        position: ray.origin + ray.direction * distance,
        normal
    })
}

fn intersect_circle(ray: &Ray, center: Point, radius: f32) -> Option<Intersection> {
    let pos = ray.origin - center;
    let b = pos.dot(ray.direction);
    let c = pos.dot(pos) - radius * radius;
    let det_sqr = b * b - c;
    if det_sqr <= 0.0 { return None }
    let det = det_sqr.sqrt();
    let mut t = -b - det;
    if t <= 0.0 || t > 9999999.0 {
        t = -b + det;
    }

    if t < 0.0 { return None }

    Some(Intersection {
        distance: t,
        position: ray.origin + ray.direction * t,
        normal: (pos + ray.direction * t).normalize()
    })
}
