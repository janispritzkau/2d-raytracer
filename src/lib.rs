pub mod line;
pub mod ray;
pub mod light;
pub mod spectrum;

pub mod prelude {
    pub use crate::ray::*;
    pub use cgmath::{EuclideanSpace, InnerSpace};
    pub use std::f32::consts::PI;

    pub type Point = cgmath::Point2<f32>;
    pub type Vector = cgmath::Vector2<f32>;
}
