use crate::prelude::*;
use rand::Rng;

pub trait Light {
    fn sample(&self, rng: &mut impl Rng) -> (Ray, f32);
}

pub struct PointLight {
    pub position: Point,
}

impl Light for PointLight {
    fn sample(&self, rng: &mut impl Rng) -> (Ray, f32) {
        let angle = rng.gen_range(0.0, 2.0 * PI);
        let ray = Ray {
            origin: self.position,
            direction: Vector::new(angle.cos(), angle.sin()),
        };
        (ray, 1.0)
    }
}

pub struct Beam {
    pub start: Point,
    pub direction: Vector,
    pub size: f32
}

impl Light for Beam {
    fn sample(&self, rng: &mut impl Rng) -> (Ray, f32) {
        let origin = if self.size == 0.0 {
            self.start
        } else {
            let up = Vector::new(-self.direction.y, self.direction.x);
            self.start + up * rng.gen_range(-0.25, 0.25) * self.size
        };
        (Ray { origin, direction: self.direction }, 1.0)
    }
}
